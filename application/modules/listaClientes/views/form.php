<div class="container mt-3 <?= $show_form ? '' : 'collapse' ?>" id="novo_cliente">
    <div class="card">
        <div class="card-header"><h4>Dados do Cliente</h4></div>
        <div class="card-body">
            <form method="POST" class="text-center border border-light p-4" enctype="multipart/form-data"  id="task-form">
            
                <div class="form-row mb-3">
                    <div class="col-md-2">
                        <div class="input-group">
                            <h5>Selecione sua foto: </h5>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="input-group">
                            <input type="file" name="foto" class="fupload form-control"/>
                        </div>
                    </div>
                </div>

                <div class="form-row mb-4">
                    <div class="col-md-4">
                        <input type="text" name="nome" value="<?= set_value('nome') ?>" class="form-control" placeholder="Nome">
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="sobrenome" value="<?= set_value('sobrenome') ?>" class="form-control" placeholder="Sobrenome">
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="telefone" value="<?= set_value('telefone') ?>" class="form-control" placeholder="Telefone" aria-describedby="defaultRegisterFormPasswordHelpBlock">
                    </div>
                </div>

                <div class="form-row ">
                    <div class="col-md-6">
                        <input type="text" name="email" value="<?= set_value('email') ?>" class="form-control mb-4" placeholder="E-mail">
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="profissao" value="<?= set_value('profissao') ?>" class="form-control" placeholder="Profissão" aria-describedby="defaultRegisterFormPasswordHelpBlock">
                    </div>
                </div>

                <div class="form-row mb-4">
                    <div class="col-md-12">
                        <input type="text" name="logradouro" value="<?= set_value('logradouro') ?>" class="form-control" placeholder="Logradouro">
                    </div>
                </div>

                <div class="form-row mb-4">
                    <div class="col-md-3">
                        <input type="text" name="cep" value="<?= set_value('cep') ?>" class="form-control mb-4" placeholder="CEP (ex: 07131-989)">
                    </div>
                    <div class="col-md-5">
                        <input type="text" name="cidade" value="<?= set_value('cidade') ?>" class="form-control mb-4" placeholder="Cidade">
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="estado" value="<?= set_value('estado') ?>" class="form-control" placeholder="Estado" aria-describedby="defaultRegisterFormPasswordHelpBlock">
                    </div>
                </div>
                
                <div class="text-center text-md-right">
                    <a class="btnupload-form btn btn-primary" onclick="document.getElementById('task-form').submit();">Enviar</a>
                </div>
            </form>
        </div>
    </div>
</div>