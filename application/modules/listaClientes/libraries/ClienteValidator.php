<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
        
class ClienteValidator extends CI_object{
    
    public function validate(){
        /**
         * A ideia era passar o upload da foto pela validação porém não funcionou
         */
        //$this->form_validation->set_rules('foto', 'Foto',  'callback_upload_foto');

        $this->form_validation->set_rules('nome', 'Nome', 'required|min_length[3]|max_length[20]');
        $this->form_validation->set_rules('sobrenome', 'Sobrenome', 'required|min_length[2]|max_length[50]');
        $this->form_validation->set_rules('telefone', 'Telefone', 'required|is_natural|min_length[9]|max_length[11]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('profissao', 'Profissão', 'required|max_length[30]');
        $this->form_validation->set_rules('logradouro', 'Logradouro', 'required|min_length[2]|max_length[100]');
        $this->form_validation->set_rules('cep', 'CEP', 'required|exact_length[9]');
        $this->form_validation->set_rules('cidade', 'cidade', 'required|max_length[28]');
        $this->form_validation->set_rules('estado', 'estado', 'required|min_length[3]|max_length[20]');
        return $this->form_validation->run();
    }

    public function upload_foto(){ // faz o upload da foto com parâmetros definidos

        $config['upload_path']          = './assets/imagens/fotos_clientes/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 200;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if($this->upload->do_upload('foto')){
            return true;
        }
        else {
            //$this->form_validation->set_message('foto', $this->upload->display_errors());
            print_r($this->upload->display_errors());
            return false;
        }
    }
}