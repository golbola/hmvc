<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/Dao.php';

class Clientes extends Dao{

    function __construct(){ 
        parent::__construct('clientes');
    }

    /**
     * Atualiza os dados de um cliente
     * @param array data: São os dados passados no formulário
     */
    public function insert($data, $table = null) {
        $cols = array('nome', 'sobrenome', 'telefone', 'email', 'profissao', 'logradouro','cep', 'cidade', 'estado', 'foto');
        $this->expected_cols($cols);
        return parent::insert($data);
    }
        
}