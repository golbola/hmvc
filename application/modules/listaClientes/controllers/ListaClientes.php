<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ListaClientes extends MY_Controller{

    public function __construct(){
        $this->load->model('ListaClientesModel', 'model');
    }

    /**
     * Página para criação de novos clientes e para listar os mesmos
     */
    public function index(){

        $data['show_form'] = $this->model->novo_cliente();
        $data['titulo'] = 'Lista de Clientes';
        $data['rotulo_botao'] = 'Novo Cliente';
        $data['form_subject'] = 'novo_cliente';
        $data['formulario'] = $this->load->view('listaClientes/form', $data, true);
        $data['lista'] = $this->model->list();

        $html = $this->load->view('main', $data, true);
        $this->show($html);
    }


    /**
     * Página para edição de clientes
     * @param int cliente_id: o id do cliente editado
     */
    public function editar($cliente_id){
        //$this->validate_id($cliente_id);
        $data = $this->model->edita_cliente($cliente_id);
        $data['show_form'] = true;
        $data['titulo'] = "Editar Cliente";
        $data['rotulo_botao'] = 'Novo Cliente';
        $data['form_subject'] = 'novo_cliente';
        $data['formulario'] = $this->load->view('listaClientes/form', $data, true);

        $html = $this->load->view('main', $data, true);
        $this->show($html);
    }

    /**
     * Página para remoção de clientes
     * @param int tarefa_id: o id do cliente removido
     */
    public function deletar($cliente_id){
        //$this->validate_id($cliente_id);
        $data = $this->model->deleta_cliente($cliente_id);
        
        $html = $this->load->view('confirm_delete', $data, true);
        $this->show($html);
    }

}