<?php
include_once APPPATH.'controllers/test/builder/TestDataBuilder.php';

class ClientesDataBuilder extends TestDataBuilder {

    public function __construct($table = 'lp2_modulo_test'){
        parent::__construct('clientes', $table);
    }

    function getData($index = -1){
        $data[0]['nome'] = 'Rafael';
        $data[0]['sobrenome'] = 'Dionizio';
        $data[0]['telefone'] = '977259861';
        $data[0]['email'] = 'rafa@hotmail.com';
        $data[0]['profissao'] = 'Estagiario';
        $data[0]['logradouro'] = 'Rua José Paulo';
        $data[0]['cep'] = '07130-538';
        $data[0]['cidade'] = 'Guarulhos';
        $data[0]['estado'] = 'São Paulo';
        $data[0]['foto'] = 'foto.jpg';

        $data[1]['nome'] = 'Letticya';
        $data[1]['sobrenome'] = 'Melo';
        $data[1]['telefone'] = '980602014';
        $data[1]['email'] = 'Let@hotmail.com';
        $data[1]['profissao'] = 'Analista';
        $data[1]['logradouro'] = 'Rua Antonio Miranda';
        $data[1]['cep'] = '07130-891';
        $data[1]['cidade'] = 'Guarulhos';
        $data[1]['estado'] = 'São Paulo';
        $data[1]['foto'] = 'perfil.jpg';

        $data[2]['nome'] = 'Rafael';
        $data[2]['sobrenome'] = 'Santos';
        $data[2]['telefone'] = '977259861';
        $data[2]['email'] = 'rafa2@hotmail.com';
        $data[2]['profissao'] = 'Analísta';
        $data[2]['logradouro'] = 'Rua José Paulo';
        $data[2]['cep'] = '07130538';
        $data[2]['cidade'] = 'Guarulhos';
        $data[2]['estado'] = 'São Paulo';
        $data[2]['foto'] = 'foto.jpg';

        return $index > -1 ? $data[$index] : $data;
    }

}