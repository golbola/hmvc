<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/listaClientes/libraries/Clientes.php';
include_once APPPATH . 'modules/listaClientes/controllers/test/builder/ClientesDataBuilder.php';

class ClientesTest extends Toast{
    private $builder;
    private $cliente;

    function __construct(){
        parent::__construct('ClientesTest');
    }

    function _pre(){
        $this->builder = new ClientesDataBuilder();
        $this->cliente = new Clientes();
    }

    function test_objetos_criados_corretamente(){
        $this->_assert_true($this->builder, "Erro na criação do builder");
        $this->_assert_true($this->cliente, "Erro na criação da tarefa");
    }

    function test_selecionado_banco_de_dados(){
        $s = $this->builder->database();
        $this->_assert_equals('lp2_modulo', $s, 'Erro na seleção do banco de teste');
    }

    function test_insere_registro_na_tabela(){
        // tenta inserir com tocdos dados corretos
        $this->builder->clean_table();
        $data = $this->builder->getData(0);
        $id1 = $this->cliente->insert($data);
        $this->_assert_equals(1, $id1, "Esperado 1, recebido $id1");

        // verifica se os dados enviados são os mesmos que estão no banco
        $task = $this->cliente->get(array('id' => 1))[0];
        $this->_assert_equals($data['nome'], $task['nome']);
        $this->_assert_equals($data['sobrenome'], $task['sobrenome']);
        $this->_assert_equals($data['telefone'], $task['telefone']);
        $this->_assert_equals($data['email'], $task['email']);
        $this->_assert_equals($data['profissao'], $task['profissao']);
        $this->_assert_equals($data['logradouro'], $task['logradouro']);
        $this->_assert_equals($data['cep'], $task['cep']);
        $this->_assert_equals($data['cidade'], $task['cidade']);
        $this->_assert_equals($data['estado'], $task['estado']);
        $this->_assert_equals($data['foto'], $task['foto']);

        // tenta inserir com vetor vazio
        $id2 = $this->cliente->insert(array());
        $this->_assert_equals(-1, $id2, "Esperado -1, recebido $id2");

        // tenta inserir sem dados
        $info = $this->builder->getData(1);
        $info['nome_inexistente'] = 1;
        $id = $this->cliente->insert($info);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");

        // tenta inserir  com dados incompletos
        $v = array('nome' => 'vetor incompleto');
        $id = $this->cliente->insert($v);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");
    }

    function test_carrega_todos_os_registros_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();
        
        // verifica se o número de registro no banco é os mesmo de resgistros inseridos
        $tasks = $this->cliente->get();
        $this->_assert_equals(3, sizeof($tasks), "Número de registros incorreto");
    }

    function test_carrega_registro_condicionalmente(){
        $this->builder->clean_table();
        $this->builder->build();

        // carrega apenas uma parte dos dados de um registro
        $task = $this->cliente->get(array('nome' => 'Rafael', 'email' => 'rafa@hotmail.com', 'id' => 1))[0];
        $this->_assert_equals('Rafael', $task['nome'], "Erro no nome");
        $this->_assert_equals('rafa@hotmail.com', $task['email'], "Erro no email");
        $this->_assert_equals(1, $task['id'], "Erro no id do cliente"); 
    }
    
    function test_atualiza_registro(){
        $this->builder->clean_table();
        $this->builder->build();

        // verifica os dados de um registro no banco
        $task1 = $this->cliente->get(array('id' => 2))[0];
        $this->_assert_equals('Letticya', $task1['nome'], "Erro no nome");
        $this->_assert_equals('Melo', $task1['sobrenome'], "Erro no sobrenome");
        $this->_assert_equals('980602014', $task1['telefone'], "Erro no telefone");
        $this->_assert_equals('Let@hotmail.com', $task1['email'], "Erro no e-mail");
        $this->_assert_equals('Analista', $task1['profissao'], "Erro na profissão");
        $this->_assert_equals('Rua Antonio Miranda', $task1['logradouro'], "Erro no logradouro");
        $this->_assert_equals('07130-891', $task1['cep'], "Erro no CEP");
        $this->_assert_equals('Guarulhos', $task1['cidade'], "Erro na cidade");
        $this->_assert_equals('São Paulo', $task1['estado'], "Erro no estado");
        $this->_assert_equals('perfil.jpg', $task1['foto'], "Erro na foto");


        // atualiza seus valores
        $task1['nome'] = 'Letticya de Melo';
        $task1['sobrenome'] = 'Aquino';
        $task1['telefone'] = '995236547';
        $task1['email'] = 'Let_melo@hotmail.com';
        $task1['profissao'] = 'Gerente';
        $task1['logradouro'] = 'Rua Bezerra Souza';
        $task1['cep'] = '09052-985';
        $task1['cidade'] = 'Trindade';
        $task1['estado'] = 'Rio de Janeiro';
        $task1['foto'] = 'perfil_2.jpg';

        $this->cliente->insert_or_update($task1);

        // verifica os dados novamente, o mesmo resgistro, e verifica se foi atualizado
        $task2 = $this->cliente->get(array('id' => 2))[0];
        $this->_assert_equals($task2['nome'], $task1['nome'], "Erro no nome");
        $this->_assert_equals($task2['sobrenome'], $task1['sobrenome'], "Erro no sobrenome");
        $this->_assert_equals($task2['telefone'], $task1['telefone'], "Erro no telefone");
        $this->_assert_equals($task2['email'], $task1['email'], "Erro no e-mail");
        $this->_assert_equals($task2['profissao'], $task1['profissao'], "Erro na profissão");
        $this->_assert_equals($task2['logradouro'], $task1['logradouro'], "Erro no logradouro");
        $this->_assert_equals($task2['cep'], $task1['cep'], "Erro no CEP");
        $this->_assert_equals($task2['cidade'], $task1['cidade'], "Erro na cidade");
        $this->_assert_equals($task2['estado'], $task1['estado'], "Erro no estado");
        $this->_assert_equals($task2['foto'], $task1['foto'], "Erro na foto");
    }
    
    function test_remove_registro_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        // verifica que o registro existe
        $task1 = $this->cliente->get(array('id' => 2))[0];
        $this->_assert_equals('Letticya', $task1['nome'], "Erro no nome");
        $this->_assert_equals('Melo', $task1['sobrenome'], "Erro no sobrenome");
        $this->_assert_equals('980602014', $task1['telefone'], "Erro no telefone");
        $this->_assert_equals('Let@hotmail.com', $task1['email'], "Erro no e-mail");
        $this->_assert_equals('Analista', $task1['profissao'], "Erro na profissão");
        $this->_assert_equals('Rua Antonio Miranda', $task1['logradouro'], "Erro no logradouro");
        $this->_assert_equals('07130-891', $task1['cep'], "Erro no CEP");
        $this->_assert_equals('Guarulhos', $task1['cidade'], "Erro na cidade");
        $this->_assert_equals('São Paulo', $task1['estado'], "Erro no estado");
        $this->_assert_equals('perfil.jpg', $task1['foto'], "Erro na foto");

        // remove o registro
        $this->cliente->delete(array('id' => 2));

        // verifica que o registro não existe mais
        $task2 = $this->cliente->get(array('id' => 2));
        $this->_assert_equals_strict(0, sizeof($task2), "Registro não foi removido");
    }

}