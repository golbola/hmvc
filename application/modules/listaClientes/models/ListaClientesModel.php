<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/component/buttons/EditDeleteButtonGroup.php';
include_once APPPATH.'libraries/component/Table.php';

class ListaClientesModel extends CI_Model{

    /**
     * Retorna a lista de clientes já cadastrada no bd
     */
    public function list(){
        $header = array('#', 'Foto' , 'Nome', 'Sobrenome', 'Telefone', 'E-mail', 'Profissão', 'Logradouro', 'CEP', 'Cidade', 'Estado');
        $this->load->library('Clientes', null, 'cliente');
        
        $this->cliente->cols(array('id', 'foto', 'nome', 'sobrenome', 'telefone', 'email', 'profissao', 'logradouro','cep', 'cidade', 'estado'));
        $data = $this->cliente->get();
        if(! sizeof($data)) return '';
        $table = new Table($data, $header);
        $table->action('listaclientes/list');
        $table->zebra_table();
        $table->use_border();
        $table->use_hover();
        $edbg = new EditDeleteButtonGroup('listaclientes');
        $table->use_action_button($edbg);
        return $table->getHTML();
    }

    /**
     * Gera um novo cliente 
     * @return boolean true caso ocorra erro de validação
     */
    public function novo_cliente(){
        if(! sizeof($_POST)) return;
        $this->load->library('ClienteValidator', null, 'valida');

        if($this->valida->validate()){
            $this->valida->upload_foto();
            $this->load->library('Clientes', null, 'cliente');
            $data = $this->input->post();
            $data['foto'] = $this->upload->data( 'file_name' );
            $this->cliente->insert($data);
        }
        else return true;
    }

    /**
     * Atualiza os dados de um cliente
     * @param int cliente_id: É o identificador do cliente
     */
    public function edita_cliente($cliente_id){
        $this->load->library('Clientes', null, 'cliente'); 
        $this->load->library('ClienteValidator', null, 'valida');
        $task = $this->cliente->get(array('id' => $cliente_id));

        if(sizeof($_POST) && $this->valida->validate()){
            $data = $this->input->post();
            $data['id'] = $cliente_id;
            $id = $this->cliente->insert_or_update($data);
            if($id) redirect('listaclientes');
        }
        else {
            foreach ($task[0] as $key => $value)
                $_POST[$key] = $value;
        }
    }

    /**
     * Elimina um cliente do bd.
     * @param int cliente_id: É o identificador do cliente
     */
    public function deleta_cliente($cliente_id) {
        $this->load->library('Clientes', null, 'cliente');
        $task = $this->cliente->get(array('id' => $cliente_id));
        
        if(sizeof($_POST)) {
            if($this->cliente->delete(array('id' => $cliente_id)))
                redirect('listaclientes');
        }
        else return $task[0];
    }

}