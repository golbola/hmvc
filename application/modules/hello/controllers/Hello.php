<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Hello extends MY_Controller{

    public function index(){
        $html = "<div class='container'>
            <h3>Hello módulo!</h3>
        </div>";
        $this->show($html);
    }

}