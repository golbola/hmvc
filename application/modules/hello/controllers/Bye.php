<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Bye extends MY_Controller{

    public function index(){
        $html = "<div class='container'>
            <h3>Bye é de corinthiano... módulo!</h3>
        </div>";
        $this->show($html);
    }

    public function fui(){
        $html = "<div class='container'>
            <h3>Fui?!</h3>
        </div>";
        $this->show($html);
    }

}