<div class="container mt-5">    
    <div class="row">
        <div class="col-md-6">
            <a href="<?= base_url('tarefa') ?>" class="btn btn-primary  <?= isset($home) ? '' : 'd-none' ?>"><i class="fas fa-home"></i></a>
        </div>
        <div class="col-md-6 text-right">
            <p>Version 1.0.0</p>
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <h2><?= $titulo ?></h2>
    </div>

    <?= validation_errors('<div class="alert alert-danger">', '</div>'); ?>

    <div class="row d-flex justify-content-end mt-5">
        <button class="btn btn-primary <?= isset($lista) ? '' : 'd-none' ?>" type="button" id="collapsebutton" 
        data-toggle="collapse" data-target="#<?= $form_subject ?>" aria-expanded="false" 
        aria-controls="<?= $form_subject ?>"><?= $rotulo_botao ?></button>
    </div>
    
    <div class="row"><?= $formulario ?></div>
    <div class="row mt-3"><?= isset($lista) ? $lista : '' ?></div>

</div>