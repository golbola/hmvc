package Program;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import test.ClienteTest;

@RunWith(Suite.class)
@SuiteClasses({
	ClienteTest.class,
})
public class TestSuite {}
