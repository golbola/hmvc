package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EditPage extends BasePage {

	public EditPage(WebDriver driver, int cliente_id) {
		super(driver, "editar/" +  cliente_id);
	}

	public ClientePage alteraDadosCliente(int cliente_id) {
		WebElement profissao = driver.findElement(By.name("profissao"));
		WebElement telefone = driver.findElement(By.name("telefone"));
		
		profissao.clear();
		telefone.clear();
		profissao.sendKeys("Presidente");		
		telefone.sendKeys("963512478");
		driver.findElement(By.className("btnupload-form")).click();
		
		return new ClientePage(driver);
	}

}
