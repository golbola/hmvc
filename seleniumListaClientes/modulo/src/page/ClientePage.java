package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ClientePage extends BasePage {

	public ClientePage(WebDriver driver) {
		super(driver, "listaClientes");
	}

	public boolean temClientes() {
		return driver.getPageSource().contains("Nome");
	}
	
	public boolean formularioOculto() {
		WebElement element = driver.findElement(By.id("novo_cliente"));
		return ! hasClass(element, "show");
	}
	
	public boolean formularioExibido() {
		WebElement button = driver.findElement(By.id("collapsebutton"));
		button.click();
		
		ExpectedCondition<Boolean> condition = ExpectedConditions.attributeContains(By.id("novo_cliente"), "class", "show");
		return new WebDriverWait(driver, 10).until(condition);
	}
	
	public boolean criaCliente() {
		if(formularioExibido()) {
			WebElement nome = driver.findElement(By.name("nome"));
			WebElement sobrenome = driver.findElement(By.name("sobrenome"));
			WebElement telefone = driver.findElement(By.name("telefone"));
			WebElement email = driver.findElement(By.name("email"));
			WebElement profissao = driver.findElement(By.name("profissao"));
			WebElement cep = driver.findElement(By.name("cep"));
			WebElement logradouro = driver.findElement(By.name("logradouro"));
			WebElement cidade = driver.findElement(By.name("cidade"));
			WebElement estado = driver.findElement(By.name("estado"));

			
			nome.sendKeys("Jo�o");
			sobrenome.sendKeys("Marcos");
			telefone.sendKeys("983541204");
			email.sendKeys("marquinho@hotmail.com");
			profissao.sendKeys("Bibliotecario");
			logradouro.sendKeys("Rua das Cerejeiras");
			cep.sendKeys("07250-258");
			cidade.sendKeys("Porto de Galinhas");
			estado.sendKeys("Pernambuco");
			
			WebElement submit = driver.findElement(By.className("btnupload-form"));
			submit.click();
			
			return driver.getPageSource().contains("2019-06-06") && 
			driver.getPageSource().contains("Teste automatizado com Selenium");
		}
		return false;
	}

	public EditPage editaCliente(int cliente_id) {
		criaCliente();
		driver.findElement(By.id("editar_1")).click();
		return new EditPage(driver, cliente_id);
	}

	public boolean comfirmaEdicao() {
		return driver.getPageSource().contains("2019-06-17") && 
		driver.getPageSource().contains("Cliente editado com sucesso");
	}

	public DeletePage removeCliente(int cliente_id) {
		criaCliente();
		driver.findElement(By.id("deletar_1")).click();
		return new DeletePage(driver, cliente_id);
	}

	public boolean comfirmaRemocao() {
		return ! temClientes();
	}
}
