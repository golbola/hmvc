package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DeletePage extends BasePage {

	public DeletePage(WebDriver driver, int cliente_id) {
		super(driver, "deletar/" + cliente_id);
	}
	
	public ClientePage removeCliente() {
		driver.findElement(By.className("delete-btn")).click(); 
		return new ClientePage(driver);
	}

}
