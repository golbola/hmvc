package test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import page.DeletePage;
import page.EditPage;
import page.ClientePage;

public class ClienteTest extends BaseTest {

	@Test
	public void criaCliente() {
		ClientePage clientePage = new ClientePage(driver);
		boolean b = clientePage.criaCliente();
		assertTrue("As tarefas n�o foram criadas", b);
	}
	
	@Test
	public void editaCliente() {
		int cliente_id = 1;
		
		ClientePage clientePage = new ClientePage(driver);
		EditPage editPage = clientePage.editaCliente(cliente_id);
		clientePage = editPage.alteraDadosCliente(cliente_id);
		boolean b = clientePage.comfirmaEdicao();
		assertTrue("Erro na edi��o de clientes", b);	
	}
	
	@Test
	public void removeCliente() {
		int cliente_id = 1;
		
		ClientePage clientePage = new ClientePage(driver);
		DeletePage deletePage = clientePage.removeCliente(cliente_id);
		clientePage = deletePage.removeCliente();
		boolean b = clientePage.comfirmaRemocao();
		assertTrue("Erro na remo��o de tarefas", b);	
	}
	
}